// import modules
const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        trim: true,
        maxlength: 30,
        minlength:3
    },
    lastName: {
        type: String,
        required: true,
        trim: true,
        maxlength: 30,
        minlength:3
    },
    username: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        maxlength: 30,
        minlength: 3,
        lowercase: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        maxlength: 50,
        minlength: 5,
        lowercase: true
    },
    password: {
        type: String,
        required:true,
        trim: true,
        maxlength: 100,
        minlength: 8
    },
    gender: {
        type: String,
        required: true,
        enum: ['مرد', 'زن', 'هیچکدام']
    },
    mobile: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    role: {
        type: String,
        default: 'blogger',
        enum: ['admin', 'blogger']
    },
    createAt: {
        type: Date,
        default: Date.now
    },
    avatar: {
        type: String,
        default: 'http://localhost:1717/uploads/profilePics/defult-profile-pic.png'
    }
});

// export module (Model)
module.exports = mongoose.model('user', UserSchema);