// import modules
const mongoose = require('mongoose');

const ArticleSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'test!!!!'],
        trim: true,
        maxlength: 20,
        minlength: 3
    },
    description: {
        type: String,
        required: true,
        trim: true,
        maxlength: 160,
        min: 3
    },
    text: {
        type: String,
        required: true,
        trim: true
    },
    image: {
        type: String
    },
    writerID: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'user'
    },
    views: {
        type: Number
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});

// Export model
module.exports = mongoose.model('Articles', ArticleSchema);