$(document).on("click", ".browse", function () {
    let file = $(this).parents().find(".file");
    file.trigger("click");
});

$('input[type="file"]').change(function (e) {
    let fileName = e.target.files[0].name;
    $("#file").val(fileName);

    let reader = new FileReader();
    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("preview").src = e.target.result;
    };
    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
});


$(document).ready(function () {

    // Add listener when click button submit my form
    $(document).on('click', '#edit-article-btn', function (event) {
        // Save tinymc text area
        tinyMCE.triggerSave();
        $('form[name="articleForm-edit"]').submit();
    });

    // My form
    $(document).on('submit', 'form[name="articleForm-edit"]', function (event) {
        event.preventDefault();
        // Get formdata
        let formData = new FormData(this);
        $.ajax({
            type: "PUT",
            url: window.location.pathname,
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                window.location.href = '../'
            },
            error: function (error) {
                console.log(error);
            }
        });

        return false;
    });
});