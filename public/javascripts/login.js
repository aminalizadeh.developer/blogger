// JQuery
/*
    I don't want to refresh page
    So I send data with ajax and
    I dont use form send request
*/

$(document).ready(function () {
    $('#login-btn').on('click', function () {
        $.ajax({
            type: "POST",
            url: "login",
            data: {
                username: $('input[name=username').val(),
                password: $('input[name=password').val()
            },
            dataType: 'json',
            success: function (response) {
                if (response) {
                    window.location.href = '../admin';
                } else {
                    window.location.href = '../dashboard';
                }
            },
            error: function (error) { 
                if (error.status !== 200) {
                    $('.alert').html(error.responseJSON.message).show();
                } 
            }
        });
    })
});