// JQuery
$(document).ready(function () {

    $(".file-upload").on('change', function (e) {
        e.preventDefault();
        $(this).closest('form').submit();
    });

    $(".upload-button").on('click', function () {
        $(".file-upload").click();
    });

    // Delegation Dashboard Container
    $('.dashboard-container').delegate('button', 'click', (event) => {

        let target = event.target;
        console.log();

        // Update Information Btn Listener
        if ($(target).attr('id') === 'update-btn') {

            const newUserInfo = {
                firstName: $(this).find('input[name=name]').val(),
                lastName: $(this).find('input[name=lastname]').val(),
                userName: $(this).find('input[name=username]').val(),
                emailAddress: $(this).find('input[name=email]').val(),
                mobileNumber: $(this).find('input[name=phoneNumber]').val(),
                gender: $(this).find('#inlineFormCustomSelect').val()
            }

            // PUT ajax request
            $.ajax({
                type: "PUT",
                url: "/profileUpdate",
                data: newUserInfo,
                dataType: "json",
                success: function (response) {
                    location.reload();
                    $('.alert-success').html(response.message).show();
                },
                error: function (error) {
                    console.log(error.status);
                    if (error.status !== 200) {
                        $('.alert-danger').html(error.responseJSON.message).show();
                    }
                }
            });
        }


    })

});