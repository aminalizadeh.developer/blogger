// JQuery
/*
    I don't want to refresh page
    So I send data with ajax and
    I dont use form send request
*/
$(document).ready(function () {
    $('#register-btn').on('click', function () {
        $.ajax({
            type: "POST",
            url: "register",
            data: {
                firstName: $('input[name=name]').val(),
                lastName: $('input[name=lastname]').val(),
                userName: $('input[name=username]').val(),
                emailAddress: $('input[name=email]').val(),
                mobileNumber: $('input[name=phoneNumber]').val(),
                passwordUser: $('input[name=password]').val(),
                gender: $('#inlineFormCustomSelect').val()
            },
            dataType: "json",
            error: function (error) {
                if (error.status !== 200) {
                    $('.alert').html(error.responseJSON.message).show();
                } else {
                    window.location.href = '/api/login';
                }
            }
        });
    });
});