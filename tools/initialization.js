// Import users model
const Users = require('../models/users');

// Add bcrypt for hash admin password
const bcrypt = require('bcrypt');

const initialization = async function () {
    try {
        // Check if admin already exist to database
        const EXIST_ADMIN = await Users.findOne({
            role: 'admin'
        });
        if (EXIST_ADMIN) {
            return console.log('The admin is already created.');
        }

        // Hash admin password
        const hashedPassword = await bcrypt.hash(process.env.ADMIN_PASSWORD, 10);
        
        // Create admin
        const ADMIN = Users({
            firstName: 'Amin',
            lastName: 'Alizadeh',
            username: 'AminAlizadeh',
            email: 'aminalizadeh.developer@gmail.com',
            password: hashedPassword,
            gender: 'مرد',
            mobile: '09394935028',
            role: 'admin'
        });

        await ADMIN.save();
        console.log('Admin created!');

    } catch (error) {
        console.log('An error occure in initialization\n' + error);
    }
}

// Export module
module.exports = initialization;