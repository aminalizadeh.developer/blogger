// import express to Creating a Server to Accept Requests
const express = require('express');
const app = express();

// import other modules
const path = require('path');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const morgan = require('morgan');

// .env requirement
require('dotenv').config();

// morgan middleware
app.use(morgan('dev'));

// Initialization admin
require('./tools/initialization')();

/*
    IN_PROD define for secure session.cookie
    If my node env is on product state and we have https
    IN_PROD will be true and my session cookie will be enable
    else secure is false because we are in development (in env defined) state and
    we dont have https!
*/
const IN_PROD = process.env.NODE_ENV === 'producte';

// Connect to mongooes database
mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});
const db = mongoose.connection;
// error handling of mongoose connection
db.on('error', error => console.error(error));
db.once('open', () => console.log('App connected successfuly to database.'));

// Parsers
app.use(cookieParser());
app.use(express.urlencoded({
    extended: true
}));

// define session for all path (middleware)
app.use(session({
    name: process.env.SESS_NAME,
    resave: false,
    saveUninitialized: false,
    secret: process.env.SESS_SECRET,
    cookie: {
        maxAge: 1000 * 60 * 60 * 6, // Six hours
        sameSite: true,
        secure: IN_PROD
    }
}));

// set view engine to ejs
app.set('view engine', 'ejs');

// use public folder static
app.use(express.static(path.join(__dirname + '/public')));

// require routes
const apiRouter = require('./routes/api');
const dashboardRouter = require('./routes/dashboard');
const profileUpdateRouter = require('./routes/profileUpdate');
const articlesRouter = require('./routes/articles');
const adminRouter = require('./routes/admin');

app.get('/', (req, res, next) => {
    const {userID} = req.session;
    res.send('session created???');
});

// use routers
app.use('/api', apiRouter);
app.use('/dashboard', dashboardRouter);
app.use('/profileUpdate', profileUpdateRouter);
app.use('/articles', articlesRouter);
app.use('/admin', adminRouter);

/*
    If a request reaches this stage,
    It means that no router has been find for it,
    So we have 404 error (Page not found!)
*/

// Create 404 error and pass to error handler
// app.use((req, res, next) => {
//     // TODO: // Create an page not found
//     const error = new Error('Page not found');
//     error.status = 404;
//     next(error);
// });

// // error handler
app.use((error, req, res, next) => {
    if (error.status === 404) {
        // TODO: create 404 page
    } else {
        // TODO: create 500 page
    }
    console.log(error);
    // return res.send('مشکلی پیش آمده لطفا مجددا تلاش کنید.');
});

// app listening
app.listen(process.env.PORT, () => {
    console.log(
        `App is running in http://localhost:${process.env.PORT}`
    );
})