// Check requirement field in register form module

const validate = (...inputValues) => {
    const inputValsLength = inputValues.length;
    for (let i = 0; i < inputValsLength; i++) {
        if (inputValues[i] === '') {
            return false;
        }
    }

    return true;
}

// Export module
exports.validate = validate;