// Phone number validation (For Iranian phone number)

const validate = (mobile) => {
    if ((/(\+98|0|98|0098)?([ ]|-|[()]){0,2}9[0-9]([ ]|-|[()]){0,2}(?:[0-9]([ ]|-|[()]){0,2}){8}/g).test(mobile)) {
        return true;
    }

    return false;
}

// Export module
exports.validate = validate;