// import models
const Users = require('../models/users');

// Check for uniqueness username
const UsernameUniqueness = async (_username) => {

    const username = await Users.findOne({
        username: _username
    });

    if (username) {
        return false;
    }

    return true;

}


// Check for uniqueness email
const EmailUniqueness = async (_email) => {
    const email = await Users.findOne({
        email: _email
    });

    if (email) {
        return false;
    }

    return true;
}

// Check for uniquness phone number
const PhoneNumberUniqueness = async (_mobile) => {
    const mobile = await Users.findOne({
        mobile: _mobile
    });

    if (mobile) {
        return false;
    }

    return true;
}

exports.UsernameUniqueness = UsernameUniqueness;
exports.EmailUniqueness = EmailUniqueness;
exports.PhoneNumberUniqueness = PhoneNumberUniqueness;