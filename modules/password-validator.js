// Password validation

function myPasswordvalidation(password) {
    let errors = [];
    // Check length of password
    if (password.length < 8 || password.length > 30) {
        errors.push('طول رمز عبور باید بیشتر از ۸ و کمتر از ۳۰ کاراکتر باشد.');
    }
    // Check password doesnot contain space character
    if (/\s/g.test(password)) {
        errors.push('رمز عبور نباید شامل کاراکتر فاصله باشد.');
    }

    return errors;
}

const validate = (password) => {
    const errorPassword = myPasswordvalidation(password);
    if (errorPassword.length > 0) {
        let errorMessage = '';
        for (let i = 0; i < errorPassword.length; i++) {
            errorMessage += (errorPassword[i]);
        }
        return errorMessage;
    }

    return false;
}

// Export module
exports.validate = validate;