// Import modules
const express = require('express');
const router = express.Router();

// Other modules
const path = require('path');

// Users collection model
const Users = require('../models/users');

// Redirect middleware
const redirectLogin = (req, res, next) => {
    if (!req.session.userID) {
        return res.redirect('/api/login');
    }
    next();
}

// Dashboard route
router.get('/', redirectLogin, async (req, res) => {
    const user_id = req.app.get('user-id');
    try {
        const user = await Users.findById(user_id);
        res.render(path.join(__dirname, '../views/dashboard.ejs'), {
            title: 'صفحه کاربری',
            firstName: user.firstName,
            lastName: user.lastName,
            flag: false,
            avatar: user.avatar
        });

    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }
});

// Exit from dashboard (log out)
router.get('/logout', (req, res, next) => {
    req.session.destroy((err) => {
        if (err) {
            err.status = 500;
            next(err);
        }

        return res.status(205).redirect('/api/login');
    });
});

// Export router
module.exports = router;