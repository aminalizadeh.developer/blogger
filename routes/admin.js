// Import modules
const express = require('express');
const router = express.Router();
const path = require('path');
const bcrypt = require('bcrypt');

// Models
const Users = require('../models/users');
const Articles = require('../models/articles');

// Redirect middleware
const redirectLogin = (req, res, next) => {
    if (!req.session.userID) {
        return res.redirect('/api/login');
    }
    next();
}
const rejectBlogger = async (req, res, next) => {
    const userID = req.app.get('user-id');
    // Get user
    const user = await Users.findById(userID);

    if (user.role !== 'admin') {
        return res.send('شما اجازه دسترسی به این بخش را ندارید!');
    }

    next();
}

// Get admin dashboard
router.get('/', redirectLogin, rejectBlogger, async (req, res, next) => {

    try {
        const AdminID = req.app.get('user-id');
        const Admin = await Users.findById(AdminID);

        return res.render(path.join(__dirname, '../views/admin.ejs'), {
            title: 'داشبورد ادمین',
            firstName: Admin.firstName,
            lastName: Admin.lastName,
            flag: false,
            articlesFlag: false
        });
    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }

});

// Get bloggers list
router.get('/bloggersList', redirectLogin, rejectBlogger, async (req, res, next) => {
    try {
        const AdminID = req.app.get('user-id');
        const Admin = await Users.findById(AdminID);
        const Bloggers = await Users.find({role: 'blogger'});
        res.render(path.join(__dirname, '../views/admin.ejs'), {
            title: 'داشبورد ادمین',
            firstName: Admin.firstName,
            lastName: Admin.lastName,
            flag: true,
            bloggers: Bloggers,
            articlesFlag: false,
            collapse: 'collapse'
        });

    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }
});

// Show blogger articles
router.get('/blogger/article/:id', redirectLogin, rejectBlogger, async (req, res, next) => {
    try {
        const AdminID = req.app.get('user-id');
        const Admin = await Users.findById(AdminID);
        const BloggerID = req.params.id;
        const BloggerArticles = await Articles.find({writerID: BloggerID});

        res.render(path.join(__dirname, '../views/admin.ejs'), {
            title: 'داشبورد ادمین',
            firstName: Admin.firstName,
            lastName: Admin.lastName,
            flag: false,
            articlesFlag: true,
            articles: BloggerArticles
        })
    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }
});

// reset password
router.get('/blogger/resetpassword/:id', redirectLogin, rejectBlogger, async (req, res, next) => {
    try {
        const AdminID = req.app.get('user-id');
        const Admin = await Users.findById(AdminID);
        const BloggerID = req.params.id;
        const Blogger = await Users.findById(BloggerID);
        const Bloggers = await Users.find({role: 'blogger'});
        const hashedPassword = await bcrypt.hash(Blogger.mobile, 10);
        await Users.findByIdAndUpdate(BloggerID, {password: hashedPassword});

        return res.render(path.join(__dirname, '../views/admin.ejs'), {
            title: 'داشبورد ادمین',
            firstName: Admin.firstName,
            lastName: Admin.lastName,
            flag: true,
            articlesFlag: false,
            collapse: '',
            bloggers: Bloggers
        });

    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }
});

// remove blogger
router.get('/blogger/remove/:id', redirectLogin, rejectBlogger, async (req, res, next) => {
    try {
        const BloggerID = req.params.id;
        await Users.findByIdAndRemove(BloggerID);

        return res.redirect('/admin/bloggersList');
    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }
});

// Export route
module.exports = router;