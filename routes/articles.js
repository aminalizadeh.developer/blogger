// Import modules
const express = require('express');
const router = express.Router();
const path = require('path');
const multer = require('multer');
const requirementField = require('../modules/form-requier-validation');

// Multer init
// Set Storage Engine
const storage = multer.diskStorage({
    destination: path.join(__dirname, '../public/uploads/articlePics'),
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

// Reject a file 
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' ||
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg') {
        cb(null, true);
    } else {
        cb('تصویر مقاله شما باید یکی از فرمت های JPEG,PNG,JPG باشد.', false);
    }
}

// Init upload
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 3 // 3Mb
    },
    fileFilter: fileFilter,
});

// Models
const Users = require('../models/users');
const Articles = require('../models/articles');

// Redirect middleware
const redirectLogin = (req, res, next) => {
    if (!req.session.userID) {
        return res.redirect('/api/login');
    }
    next();
}

// Show article route
router.get('/', redirectLogin, async (req, res) => {
    try {
        // Get ID of user
        const user_id = req.app.get('user-id');
        const user = await Users.findById(user_id);

        const userArticles = await Articles.find({
            writerID: user_id
        });

        res.render(path.join(__dirname, '../views/articles.ejs'), {
            title: 'مقالات',
            firstName: user.firstName,
            lastName: user.lastName,
            flag: true,
            avatar: user.avatar,
            articles: userArticles
        });
    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }

});

// Get new article page
router.get('/new', redirectLogin, async (req, res) => {

    try {
        // Get ID of user
        const user_id = req.app.get('user-id');

        // empty article
        const article = new Articles();
        const user = await Users.findById(user_id);

        res.render(path.join(__dirname, '../views/newArticle'), {
            title: 'مقالات',
            pageTitle: 'افزودن مقاله جدید',
            firstName: user.firstName,
            lastName: user.lastName,
            flag: true,
            avatar: user.avatar,
            errorMessage: '',
            collapse: 'collapse',
            article: article
        });
    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }
});

// Create new article
router.post('/new', redirectLogin, upload.single('articlePic'), async (req, res) => {

    // Get ID of user
    const user_id = req.app.get('user-id');
    const user = await Users.findById(user_id);

    try {
        const {
            title,
            description,
            mce_0
        } = req.body;

        // Check requirments
        if (!requirementField.validate(title, description, mce_0)) {
            throw new Error('مقاله شما حتما باید دارای عنوان و خلاصه ای از مقاله و همچنین متن اصلی باشد.')
        }

        // Check length of title
        if (title.length < 3 || title.length > 20) {
            throw new Error('عنوان باید دارای حداقل ۳ حرف و حداکثر ۲۰ حرف باشد.');
        }

        // Check lenght of description
        if (description.length < 3 || description.length > 160) {
            throw new Error('خلاصه مقاله باید دارای حداقل ۳ حرف و حداکثر ۱۶۰ حرف باشد.');
        }

        // Check image
        let imagePath = '';
        if (req.file === undefined) {
            imagePath = 'http://localhost:1717/uploads/articlePics/default.jpg';
        } else {
            imagePath = 'http://localhost:1717/uploads/articlePics/' + req.file.filename;
        }

        const NewArticle = new Articles({
            title: title,
            description: description,
            text: mce_0,
            image: imagePath,
            writerID: user_id,
        });

        await NewArticle.save();

        return res.redirect('/articles');

    } catch (error) {
        // empty article
        const article = new Articles();

        res.render(path.join(__dirname, '../views/newArticle'), {
            title: 'مقالات',
            pageTitle: 'افزودن مقاله جدید',
            firstName: user.firstName,
            lastName: user.lastName,
            flag: true,
            avatar: user.avatar,
            errorMessage: error.message,
            collapse: '',
            article: article
        });
    }

});

// Show article
router.get('/show/:id', redirectLogin, async (req, res) => {

    try {
        // Get ID of user
        const user_id = req.app.get('user-id');
        const user = await Users.findById(user_id);

        // Get article id
        const articleID = req.params.id;
        const article = await Articles.findById(articleID);

        res.render(path.join(__dirname, '../views/showArticle'), {
            title: article.title,
            firstName: user.firstName,
            lastName: user.lastName,
            flag: true,
            avatar: user.avatar,
            article: article
        });
    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }

});

// Remove article
router.get('/remove/:id', redirectLogin, async (req, res) => {

    try {
        // Get article id
        const articleID = req.params.id;
        
        // Get ID of user
        const user_id = req.app.get('user-id');
        // Grt article for find writer id
        const ArticleValid = await Articles.findById(articleID);
        // If user be article owner
        if (String(user_id) === String(ArticleValid.writerID))  {
            // Delete by blogger
            await Articles.findByIdAndRemove(articleID);
            console.log('Delete by blogger');
            return res.redirect('/articles');
        } else {
            // Delete by admin
            await Articles.findByIdAndRemove(articleID);
            return res.redirect('../../admin/blogger/article/' + user_id);
        }

    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }

    // redirect
    res.redirect('/articles');
});

// Get Edit article
router.get('/edit/:id', redirectLogin, async (req, res) => {

    try {
        // Get article id
        const articleID = req.params.id;
        const article = await Articles.findById(articleID);

        const user_id = req.app.get('user-id');
        const user = await Users.findById(user_id);

        res.render(path.join(__dirname, '../views/editArticle'), {
            title: 'ویرایش مقاله',
            pageTitle: 'ویرایش مقاله ',
            btnTitle: 'ویرایش',
            firstName: user.firstName,
            lastName: user.lastName,
            flag: true,
            avatar: user.avatar,
            errorMessage: '',
            collapse: 'collapse',
            article: article
        });
    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }
});

// Edit article
router.put('/edit/:id', redirectLogin, upload.single('articlePic'), async (req, res) => {
    // Get article id
    const articleID = req.params.id;
    const {
        title,
        description,
        mce_0
    } = req.body;

    try {

        // Check requirments
        if (!requirementField.validate(title, description, mce_0)) {
            throw new Error('مقاله شما حتما باید دارای عنوان و خلاصه ای از مقاله و همچنین متن اصلی باشد.')
        }

        // Check length of title
        if (title.length < 3 || title.length > 20) {
            throw new Error('عنوان باید دارای حداقل ۳ حرف و حداکثر ۲۰ حرف باشد.');
        }

        // Check lenght of description
        if (description.length < 3 || description.length > 160) {
            throw new Error('خلاصه مقاله باید دارای حداقل ۳ حرف و حداکثر ۱۶۰ حرف باشد.');
        }

        // Check image
        let imagePath = '';
        const article = await Articles.findById(articleID);

        if (req.file === undefined) {
            await Articles.findByIdAndUpdate(article, {
                title: title,
                description: description,
                text: mce_0
            });
        } else {
            imagePath = 'http://localhost:1717/uploads/articlePics/' + req.file.filename;
            await Articles.findByIdAndUpdate(article, {
                title: title,
                description: description,
                text: mce_0,
                image: imagePath,
            });
        }

        // Send response 
        res.status(200).end();

    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }

});

// Export route
module.exports = router;