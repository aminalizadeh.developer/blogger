// import express
const express = require('express');

// import other modules
const path = require('path');
const emailValidator = require('email-validator');
const RequireValidator = require('../modules/form-requier-validation');
const MobileValidator = require('../modules/mobile-validator');
const PasswordValidator = require('../modules/password-validator');
const FormUniqueness = require('../modules/form-uniqueness');
const bcrypt = require('bcrypt');

// import models
const Users = require('../models/users');

// create router
const router = express.Router();

// redirect middlewares
const redirectDashboard = (req, res, next) => {
    if (req.session.userID) {
        return res.redirect('../dashboard');
    }

    next();
}

// __________ register __________

// Get register page
router.get('/register', redirectDashboard, (req, res) => {
    res.render(path.join(__dirname, '../views/register.ejs'), {
        title: 'ثبت نام'
    });
});

// Post data of register page and save them to database
router.post('/register', redirectDashboard, async (req, res, next) => {
    // Get request body data
    const {
        firstName,
        lastName,
        userName,
        emailAddress,
        mobileNumber,
        passwordUser,
        gender
    } = req.body;

    // Form validation inputs
    try {

        // Check requirement field in register form
        if (!RequireValidator.validate(firstName, lastName, userName, emailAddress, mobileNumber, passwordUser, gender)) {
            return res.status(400).send({
                message: 'لطفا تمامی اطلاعات خواسته شده در فرم ثبت نام را تکمیل فرمایید.'
            });
        }

        // Email validation
        if (!emailValidator.validate(emailAddress)) {
            return res.status(422).send({
                message: 'آدرس ایمیل وارد شده معتبر نمی باشد!'
            });
        }

        // Phone number validation (For Iranian phone number)
        if (!MobileValidator.validate(mobileNumber)) {
            return res.status(422).send({
                message: 'شماره موبایل وارد شده معتبر نمی باشد!'
            });
        }

        // Password validation
        if (PasswordValidator.validate(passwordUser)) {
            return res.status(401).send({
                message: PasswordValidator.validate(passwordUser)
            });
        }

        // Check uniqueness
        if (!await FormUniqueness.UsernameUniqueness(userName)) {
            return res.status(409).send({
                message: 'این نام کاربری قبلا ثبت شده است لطفا ان را تغییر دهید.'
            });
        }

        if (!await FormUniqueness.EmailUniqueness(emailAddress)) {
            return res.status(409).send({
                message: 'کاربری با این آدرس ایمیل قبلا ثبت نام کرده است!'
            });
        }

        if (!await FormUniqueness.PhoneNumberUniqueness(mobileNumber)) {
            return res.status(409).send({
                message: 'کاربری با این شماره موبایل قبلا ثبت نام کرده است!'
            });
        }

        /* 
            Using bcrypt for hashing password to be secure user's password
            Befor saveing user to data base in need to hash password
            I use salt for be more secure user's password hash
        */
        const hashedPassword = await bcrypt.hash(passwordUser, 10);
        const NEW_USER = new Users({
            firstName: firstName,
            lastName: lastName,
            username: userName,
            email: emailAddress,
            password: hashedPassword,
            gender: gender,
            mobile: mobileNumber
        });

        await NEW_USER.save();

        // redirect to login page
        res.status(200).redirect('login');

    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }
});


// __________ login __________

// Render login page
router.get('/login', redirectDashboard, (req, res) => {
    res.render(path.join(__dirname, '../views/login.ejs'), {
        title: 'ورود به حساب کاربری'
    });
});

// Post login page
router.post('/login', redirectDashboard, async (req, res) => {
    if (!req.body.username || !req.body.password) {
        return res.status(400).send({
            message: 'لطفا نام کاربری و رمز عبور خود را وارد کنید.'
        })
    }

    const {
        username,
        password
    } = req.body;

    checkUser(username, password);

    async function checkUser(username, password) {
        try {
            const user = await Users.findOne({
                username: username
            });

            if (user) {
                const match = await bcrypt.compare(password, user.password);

                if (match) {
                    // Save session as cookie in browser
                    req.session.userID = true;
                    req.app.set('user-id', user._id);

                    // Admin log in
                    if (user.role === 'admin') {
                        console.log('Admin Enter');
                        return res.send(true);
                    }
                    console.log('User Enter');
                    return res.send(false);
                    // return res.status(200).redirect('../dashboard');
                }
            }

            return res.status(401).send({
                message: 'کاربری با این مشخصات و جود ندارد.'
            });
        } catch (error) {
            // Internal server error 500
            error.status = 500;
            next(error);
        }
    }

});



// export router
module.exports = router;