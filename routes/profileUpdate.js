// Import modules
const express = require('express');
const router = express.Router();

// Other modules
const path = require('path');
const emailValidator = require('email-validator');
const RequireValidator = require('../modules/form-requier-validation');
const MobileValidator = require('../modules/mobile-validator');
const FormUniqueness = require('../modules/form-uniqueness');
const multer = require('multer');

// Multer init
// Set Storage Engine
const storage = multer.diskStorage({
    destination: path.join(__dirname, '../public/uploads/profilePics'),
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

// Reject a file 
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' ||
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg') {
        cb(null, true);
    } else {
        cb('تصویر حساب کاربری شما باید یکی از فرمت های JPEG,PNG,JPG باشد.', false);
    }
}

// Init upload
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 3 // 3Mb
    },
    fileFilter: fileFilter,
});

// Users collection model
const Users = require('../models/users');

// Redirect middleware
const redirectLogin = (req, res, next) => {
    if (!req.session.userID) {
        return res.redirect('/api/login');
    }
    next();
}


// Edite user information
router.get('/', redirectLogin, async (req, res) => {
    const user_id = req.app.get('user-id');
    try {
        const user = await Users.findById(user_id);

        res.render(path.join(__dirname, '../views/dashboard.ejs'), {
            title: 'ویرایش حساب کاربری',
            flag: true,
            firstName: user.firstName,
            lastName: user.lastName,
            username: user.username,
            email: user.email,
            phone: user.mobile,
            gender: user.gender,
            avatar: user.avatar
        });

    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }
});

// Update profile
router.put('/', redirectLogin, async (req, res) => {
    const {
        firstName,
        lastName,
        userName,
        emailAddress,
        mobileNumber,
        gender
    } = req.body;

    // Get ID of user from req of app
    const user_id = req.app.get('user-id');

    // Get user befor update for check uniqueness
    const user = await Users.findById(user_id);

    // Check Validation Form
    try {

        // Check requirement field in register form
        if (!RequireValidator.validate(firstName, lastName, userName, emailAddress, mobileNumber, gender)) {
            return res.status(400).send({
                message: 'لطفا تمامی اطلاعات خواسته شده در فرم ثبت نام را تکمیل فرمایید.'
            });
        }

        // Email validation
        if (!emailValidator.validate(emailAddress)) {
            return res.status(422).send({
                message: 'آدرس ایمیل وارد شده معتبر نمی باشد!'
            });
        }

        // Phone number validation (For Iranian phone number)
        if (!MobileValidator.validate(mobileNumber)) {
            return res.status(422).send({
                message: 'شماره موبایل وارد شده معتبر نمی باشد!'
            });
        }

        // Check uniqueness
        if (!(await FormUniqueness.UsernameUniqueness(userName)) && user.username !== userName) {
            return res.status(409).send({
                message: 'این نام کاربری قبلا ثبت شده است لطفا ان را تغییر دهید.'
            });
        }

        if ((!await FormUniqueness.EmailUniqueness(emailAddress)) && user.email !== emailAddress) {
            return res.status(409).send({
                message: 'کاربری با این آدرس ایمیل قبلا ثبت نام کرده است!'
            });
        }

        if ((!await FormUniqueness.PhoneNumberUniqueness(mobileNumber)) && user.mobile !== mobileNumber) {
            return res.status(409).send({
                message: 'کاربری با این شماره موبایل قبلا ثبت نام کرده است!'
            });
        }

        // Update From Database
        await Users.findByIdAndUpdate(user_id, {
            firstName: firstName,
            lastName: lastName,
            username: userName,
            email: emailAddress,
            gender: gender,
            mobile: mobileNumber
        });

        return res.status(200).send({
            message: 'اطلاعات شما با موفقیت ذخیره شد.'
        });

    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }
});

// Upload Avatar route
router.post('/uploadAvatar', redirectLogin, upload.single('avatar'), async (req, res) => {
    const user_id = req.app.get('user-id');
    try {
        await Users.findByIdAndUpdate(user_id, {
            avatar: 'http://localhost:1717/uploads/profilePics/' + req.file.filename
        });
    } catch (error) {
        // Internal server error 500
        error.status = 500;
        next(error);
    }

    res.redirect('/dashboard');
})

// Export router
module.exports = router;